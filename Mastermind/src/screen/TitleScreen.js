import { Container, utils, Graphics, Text } from "pixi.js";

var circleColors = [
    0xFF0000, //Red
    0x00FF00, //Green
    0x0000FF, //Blue
    0xFFFF00, //Yellow
    0xFF00FF, //Magenta
    0x00FFFF, //Cyan
    0x117A65, //Dark Green
    0x800000, //Dark Red
];

var hintColors = [
    0xaaaaaa, //Default
    0xFFFFFF, //White
    0x000000, //Black
]

var hints = [];

var hintCount = 1;

function randomNoRepeats(array) {
    var copy = array.slice(0);
    return function() {
      if (copy.length < 1) { copy = array.slice(0); }
      var index = Math.floor(Math.random() * copy.length);
      var item = copy[index];
      copy.splice(index, 1);
      return item;
    };
  }

  var chooser = randomNoRepeats(circleColors)

  var secret1 = chooser();
  var secret2 = chooser();
  var secret3 = chooser();
  var secret4 = chooser();

var circleColorNum1 = 0;
var circleColorNum2 = 0;
var circleColorNum3 = 0;
var circleColorNum4 = 0;

var black = 0;
var white = 0;
var rounds = 1;

var columnPos = 850;
var hintPosY = 245;

class TitleScreen extends Container{
    constructor(){
        super();

        this.interactive = true;

        var cache = utils.TextureCache;

        this.bgGraphics = new Graphics();
        this.bgGraphics.beginFill(0xffd677);
        this.bgGraphics.drawRect(0,25,900,300);
        this.bgGraphics.endFill();
        this.addChild(this.bgGraphics);

        this.circle1 = new Graphics();
        this.circle1.beginFill(circleColors[circleColorNum1]);
        this.circle1.drawCircle(columnPos,50,15);
        this.circle1.endFill();
        this.addChild(this.circle1);

        this.circle2 = new Graphics();
        this.circle2.beginFill(circleColors[circleColorNum2]);
        this.circle2.drawCircle(columnPos,100,15);
        this.circle2.endFill();
        this.addChild(this.circle2);

        this.circle3 = new Graphics();
        this.circle3.beginFill(circleColors[circleColorNum3]);
        this.circle3.drawCircle(columnPos,150,15);
        this.circle3.endFill();
        this.addChild(this.circle3);

        this.circle4 = new Graphics();
        this.circle4.beginFill(circleColors[circleColorNum4]);
        this.circle4.drawCircle(columnPos,200,15);
        this.circle4.endFill();
        this.addChild(this.circle4);

        this.checkButton = new Graphics()
        this.checkButton.beginFill(0xFFFFFF);
        this.checkButton.drawRect(750,350,120,60);
        this.checkButton.endFill();
        this.addChild(this.checkButton);

        var checkButtonText = new Text("Check",{
            fill : [0x000000],
            fontSize : 36,
        });
        checkButtonText.x = 760;
        checkButtonText.y = 360;
        this.addChild(checkButtonText);

        var roundText = new Text(rounds,{
            fill : [0xFFFFFF],
            fontSize : 24,
        });
        roundText.x = columnPos;
        roundText.y = 0;
        this.addChild(roundText);
    }

    mousedown(e){
        
        if(this.bgGraphics.getBounds().contains(e.data.global.x, e.data.global.y))
        {
        console.log("Image Clicked");
        }

        if(this.circle1.getBounds().contains(e.data.global.x, e.data.global.y))
        {
            if(circleColorNum1 + 1 == 8){
                circleColorNum1 = 0;
            }
            else{
                circleColorNum1++;
            }
        this.circle1.beginFill(circleColors[circleColorNum1]);
        this.circle1.drawCircle(columnPos,50,15);
        this.circle1.endFill();
        console.log("Changed Color");
        }

        if(this.circle2.getBounds().contains(e.data.global.x, e.data.global.y))
        {
            if(circleColorNum2 + 1 == 8){
                circleColorNum2 = 0;
            }
            else{
                circleColorNum2++;
            }
        this.circle2.beginFill(circleColors[circleColorNum2]);
        this.circle2.drawCircle(columnPos,100,15);
        this.circle2.endFill();
        console.log("Changed Color");
        }

        if(this.circle3.getBounds().contains(e.data.global.x, e.data.global.y))
        {
            if(circleColorNum3 + 1 == 8){
                circleColorNum3 = 0;
            }
            else{
                circleColorNum3++;
            }
        this.circle3.beginFill(circleColors[circleColorNum3]);
        this.circle3.drawCircle(columnPos,150,15);
        this.circle3.endFill();
        console.log("Changed Color");
        }

        if(this.circle4.getBounds().contains(e.data.global.x, e.data.global.y))
        {
            if(circleColorNum4 + 1 == 8){
                circleColorNum4 = 0;
            }
            else{
                circleColorNum4++;
            }
        this.circle4.beginFill(circleColors[circleColorNum4]);
        this.circle4.drawCircle(columnPos,200,15);
        this.circle4.endFill();
        console.log("Changed Color");
        }

        if(this.checkButton.getBounds().contains(e.data.global.x, e.data.global.y))
        {
        console.log("Checked Colors");

            if(secret1 === circleColors[circleColorNum1])
            {
                console.log("White");
                white++;
            }
            else if(secret1 === circleColors[circleColorNum2])
            {
                console.log("Black");
                black++;
            }
            else if(secret1 === circleColors[circleColorNum3])
            {
                console.log("Black");
                black++;
            }
            else if(secret1 === circleColors[circleColorNum4])
            {
                console.log("Black");
                black++;
            }
            if(secret2 === circleColors[circleColorNum2])
            {
                console.log("White");
                white++;
            }
            else if(secret2 === circleColors[circleColorNum1])
            {
                console.log("Black");
                black++;
            }
            else if(secret2 === circleColors[circleColorNum3])
            {
                console.log("Black");
                black++;
            }
            else if(secret2 === circleColors[circleColorNum4])
            {
                console.log("Black");
                black++;
            }
            if(secret3 === circleColors[circleColorNum3])
            {
                console.log("White");
                white++;
            }
            else if(secret3 === circleColors[circleColorNum1])
            {
                console.log("Black");
                black++;
            }
            else if(secret3 === circleColors[circleColorNum2])
            {
                console.log("Black");
                black++;
            }
            else if(secret3 === circleColors[circleColorNum4])
            {
                console.log("Black");
                black++;
            }
            if(secret4 === circleColors[circleColorNum4])
            {
                console.log("White");
                white++;
            }
            else if(secret4 === circleColors[circleColorNum1])
            {
                console.log("Black");
                black++;
            }
            else if(secret4 === circleColors[circleColorNum2])
            {
                console.log("Black");
                black++;
            }
            else if(secret4 === circleColors[circleColorNum3])
            {
                console.log("Black");
                black++;
            }

            if(white === 4 || rounds === 12)
            {
                console.log("Win");
                this.guessCircle1 = new Graphics();
                this.guessCircle1.beginFill(secret1);
                this.guessCircle1.drawCircle(50,350,15);
                this.guessCircle1.endFill();
                this.addChild(this.guessCircle1);

                this.guessCircle2 = new Graphics();
                this.guessCircle2.beginFill(secret2);
                this.guessCircle2.drawCircle(100,350,15);
                this.guessCircle2.endFill();
                this.addChild(this.guessCircle2);

                this.guessCircle3 = new Graphics();
                this.guessCircle3.beginFill(secret3);
                this.guessCircle3.drawCircle(150,350,15);
                this.guessCircle3.endFill();
                this.addChild(this.guessCircle3);

                this.guessCircle4 = new Graphics();
                this.guessCircle4.beginFill(secret4);
                this.guessCircle4.drawCircle(200,350,15);
                this.guessCircle4.endFill();
                this.addChild(this.guessCircle4);
            }
            else if(circleColors[circleColorNum1] === circleColors[circleColorNum2] ||
                    circleColors[circleColorNum1] === circleColors[circleColorNum3] ||
                    circleColors[circleColorNum1] === circleColors[circleColorNum4] ||
                    circleColors[circleColorNum2] === circleColors[circleColorNum3] ||
                    circleColors[circleColorNum2] === circleColors[circleColorNum4] ||
                    circleColors[circleColorNum3] === circleColors[circleColorNum4])
            {
                console.log("Duplicates not allowed");
            }

            else{
                
                if(white > 0)
                {
                    for(hintCount = 1; white >= hintCount; hintCount++)
                    {
                        hints[hintCount - 1] = new Graphics();
                        hints[hintCount - 1].beginFill(hintColors[1]);
                        hints[hintCount - 1].drawCircle(columnPos,hintPosY,7);
                        hints[hintCount - 1].endFill();
                        this.addChild(hints[hintCount - 1]);
                        hintPosY = hintPosY + 21;
                    }
                }
                
                if(black > 0)
                {
                    for(hintCount = 1; black >= hintCount; hintCount++)
                    {
                        hints[hintCount - 1 + white] = new Graphics();
                        hints[hintCount - 1 + white].beginFill(hintColors[2]);
                        hints[hintCount - 1 + white].drawCircle(columnPos,hintPosY,7);
                        hints[hintCount - 1 + white].endFill();
                        this.addChild(hints[hintCount - 1 + white]);
                        hintPosY = hintPosY + 21;
                    }
                }

                if(black < 4 || white < 4)
                {
                    for(hintCount = 1; 4 - (black + white) >= hintCount; hintCount++)
                    {
                        hints[hintCount - 1 + white + black] = new Graphics();
                        hints[hintCount - 1 + white + black].beginFill(hintColors[0]);
                        hints[hintCount - 1 + white + black].drawCircle(columnPos,hintPosY,7);
                        hints[hintCount - 1 + white + black].endFill();
                        this.addChild(hints[hintCount - 1 + white + black]);
                        hintPosY = hintPosY + 21;
                    }
                }
                
                hintPosY = 245;
                white = 0;
                black = 0;
                rounds++;
                columnPos = columnPos - 75;

                this.circle1 = new Graphics();
                this.circle1.beginFill(circleColors[circleColorNum1]);
                this.circle1.drawCircle(columnPos,50,15);
                this.circle1.endFill();
                this.addChild(this.circle1);

                this.circle2 = new Graphics();
                this.circle2.beginFill(circleColors[circleColorNum2]);
                this.circle2.drawCircle(columnPos,100,15);
                this.circle2.endFill();
                this.addChild(this.circle2);

                this.circle3 = new Graphics();
                this.circle3.beginFill(circleColors[circleColorNum3]);
                this.circle3.drawCircle(columnPos,150,15);
                this.circle3.endFill();
                this.addChild(this.circle3);

                this.circle4 = new Graphics();
                this.circle4.beginFill(circleColors[circleColorNum4]);
                this.circle4.drawCircle(columnPos,200,15);
                this.circle4.endFill();
                this.addChild(this.circle4);

                var roundText = new Text(rounds,{
                    fill : [0xFFFFFF],
                    fontSize : 24,
                });
                roundText.x = columnPos;
                roundText.y = 0;
                this.addChild(roundText);
            }
        }
    }
}

export default TitleScreen;